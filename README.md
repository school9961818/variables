## Prerequisites

[Simple output](https://gitlab.com/school9961818/simpleoutput)

## Goal

* Code
  * variables
  * int/bool/string 
* VisualStudio
  * Multiple projects

## Diffuculty

* Beginners
* 1-2x45 min

## Instructions

Naklonujte si projekt do počítače a postupně vyřešte zadání ve všech Projektech

Vždy se spouští jen jeden (aktuální) projekt. Pokud chcete přepnout mezi projekty, můžete to udělat v menu nahoře

![Top menu](images/startup_top_menu.jpg)

případně pravým kliknutím na projekt, který chcete spustit

![Right click](images/startup_right_click.jpg)

## Feedback

https://forms.gle/8GDYJEMRKDyWcY3KA

## Where to next

[Conditions](https://gitlab.com/school9961818/conditions)
